@echo off

mkdir build
cd build

:: remove -GL from CXXFLAGS
set "CXXFLAGS=-MD"

set BUILD_CONFIG=Release
set PYTHON_MAJOR_VERSION=%PY_VER:~0,1%

cmake ..  -G "Visual Studio 17 2022" ^
    -Wno-dev ^
    -DCMAKE_BUILD_TYPE=%BUILD_CONFIG% ^
    -DCMAKE_PREFIX_PATH="%LIBRARY_PREFIX%" ^
    -DCMAKE_INSTALL_LIBDIR="Library/lib" ^
    -DCMAKE_INSTALL_BINDIR="Library/bin" ^
    -DCMAKE_INSTALL_INCLUDEDIR="Library/include" ^
    -DCMAKE_INSTALL_DATAROOTDIR="Library/share" ^
    -DCMAKE_INSTALL_PREFIX="%PREFIX%" ^
    -DVTK_PYTHON_SITE_PACKAGES_SUFFIX="Lib/site-packages" ^
    -DVTK_BUILD_TESTING:BOOL=OFF ^
    -DBUILD_SHARED_LIBS:BOOL=ON ^
    -DVTK_WRAP_PYTHON:BOOL=ON ^
    -DVTK_PYTHON_VERSION:STRING="%PYTHON_MAJOR_VERSION%" ^
    -DVTK_BUILD_DOCUMENTATION:BOOL=OFF ^
    -DVTK_BUILD_EXAMPLES:BOOL=OFF ^
    -DPython3_FIND_STRATEGY=LOCATION ^
    -DPython3_ROOT_DIR="%PREFIX%" ^
    -DVTK_HAS_FEENABLEEXCEPT:BOOL=OFF ^
    -DVTK_SMP_IMPLEMENTATION_TYPE:STRING=TBB ^
    -DVTK_DATA_EXCLUDE_FROM_ALL:BOOL=ON ^
    -DVTK_GROUP_ENABLE_Qt=YES ^
    -DVTK_QT_VERSION=6 ^
    -DVTK_MODULE_ENABLE_VTK_GUISupportQt=YES ^
    -DVTK_MODULE_ENABLE_VTK_GUISupportQtQuick=YES ^
    -DVTK_MODULE_ENABLE_VTK_RenderingOpenGL2=YES ^
    -DVTK_MODULE_ENABLE_VTK_RenderingExternal=YES ^
    -DVTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2=YES ^
    -DVTK_MODULE_ENABLE_VTK_RenderingContextOpenGL2=YES ^
    -DVTK_MODULE_ENABLE_VTK_RenderingVolumeOpenGL2=YES ^
    -DVTK_MODULE_ENABLE_VTK_FiltersParallelDIY2:STRING=YES ^
    -DVTK_MODULE_ENABLE_VTK_PythonInterpreter:BOOL=NO ^
    -DVTK_DEFAULT_RENDER_WINDOW_OFFSCREEN:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_libharu:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_pegtl:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_exprtk:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_fmt:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_cgns:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_ioss:BOOL=OFF ^
    -DVTK_MODULE_USE_EXTERNAL_VTK_verdict:BOOL=OFF ^
    -DVTK_OPENGL_HAS_OSMESA:BOOL=OFF ^
    -DLZMA_LIBRARY="%LIBRARY_PREFIX%/lib/liblzma.lib"  ^
    -DQMLPLUGINDUMP_EXECUTABLE="${BUILD_PREFIX}/lib/qt6/bin/qmlplugindump"

if errorlevel 1 exit 1

cmake --build . --config %BUILD_CONFIG% --parallel --target install

set egg_info=%SP_DIR%\vtk-%PKG_VERSION%.egg-info
echo>%egg_info% Metadata-Version: 2.1
echo>>%egg_info% Version: %PKG_VERSION%
echo>>%egg_info% Summary: VTK is an open-source toolkit for 3D computer graphics, image processing, and visualization
echo>>%egg_info% Platform: UNKNOWN

if errorlevel 1 exit 1
