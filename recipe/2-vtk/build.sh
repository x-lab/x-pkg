#!/usr/bin/env sh

mkdir package_build
pushd package_build


VTK_ARGS=()
if [[ "${target_platform}" == linux-* ]]; then
    VTK_ARGS+=(
        "-DVTK_USE_X:BOOL=ON"
    )
elif [[ "${target_platform}" == osx-* ]]; then
    VTK_ARGS+=(
    "-DVTK_USE_X:BOOL=OFF"
        "-DVTK_USE_COCOA:BOOL=ON"
        "-DCMAKE_OSX_SYSROOT:PATH=${CONDA_BUILD_SYSROOT}"
    )
    export CFLAGS="-Wno-incompatible-pointer-types"
fi

if test "$CONDA_BUILD_CROSS_COMPILATION" = "1"
then
  CMAKE_ARGS="${CMAKE_ARGS} -DQT_HOST_PATH=${BUILD_PREFIX}"
fi

ARCH=`arch`
if [ $ARCH = "arm64" ] || [ $ARCH = "aarch64" ]; then
  export QT_HOST_PATH=$PREFIX
fi

cmake -LAH .. -G"Ninja" \
    -DCMAKE_BUILD_TYPE="${BUILD_TYPE}" \
    -DCMAKE_PREFIX_PATH:PATH="${PREFIX}" \
    -DCMAKE_INSTALL_PREFIX:PATH="${PREFIX}" \
    -DCMAKE_INSTALL_RPATH:PATH="${PREFIX}/lib" \
    -DCMAKE_INSTALL_LIBDIR="lib" \
    -DVTK_BUILD_DOCUMENTATION:BOOL=OFF \
    -DVTK_BUILD_TESTING:BOOL=OFF \
    -DVTK_BUILD_EXAMPLES:BOOL=OFF \
    -DBUILD_SHARED_LIBS:BOOL=ON \
    -DVTK_GROUP_ENABLE_Qt=YES \
    -DVTK_QT_VERSION=6 \
    -DVTK_MODULE_ENABLE_VTK_GUISupportQt=YES \
    -DVTK_MODULE_ENABLE_VTK_GUISupportQtQuick=YES \
    -DVTK_MODULE_ENABLE_VTK_RenderingOpenGL2=YES \
    -DVTK_MODULE_ENABLE_VTK_RenderingExternal=YES \
    -DVTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2=YES \
    -DVTK_MODULE_ENABLE_VTK_RenderingContextOpenGL2=YES \
    -DVTK_MODULE_ENABLE_VTK_RenderingVolumeOpenGL2=YES \
    -DVTK_MODULE_ENABLE_VTK_FiltersParallelDIY2:STRING=YES \
    -DVTK_WRAP_PYTHON:BOOL=ON \
    -DVTK_MODULE_ENABLE_VTK_PythonInterpreter:BOOL=NO \
    -DVTK_PYTHON_VERSION:STRING=3 \
    -DPython3_EXECUTABLE=$PYTHON \
    -DVTK_HAS_FEENABLEEXCEPT:BOOL=OFF \
    -DVTK_DEFAULT_RENDER_WINDOW_OFFSCREEN:BOOL=OFF \
    -DVTK_OPENGL_HAS_OSMESA:BOOL=OFF \
    -DQMLPLUGINDUMP_EXECUTABLE="${BUILD_PREFIX}/lib/qt6/bin/qmlplugindump" \
    "${VTK_ARGS[@]}"

cmake --build . --parallel --target install

# egg-info to inform pip that vtk is installed

cat > $SP_DIR/vtk-$PKG_VERSION.egg-info <<FAKE_EGG
Metadata-Version: 2.1
Name: vtk
Version: $PKG_VERSION
Summary: VTK is an open-source toolkit for 3D computer graphics, image processing, and visualization
Platform: UNKNOWN
FAKE_EGG
