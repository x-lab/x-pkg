#!/usr/bin/env sh

mkdir package_build
pushd package_build
export LANG=C
export LC_ALL=C

if [[ "$c_compiler" == "gcc" ]]; then
  export PATH="${PATH}:${BUILD_PREFIX}/${HOST}/sysroot/usr/lib:${BUILD_PREFIX}/${HOST}/sysroot/usr/include"
fi

../configure \
    -prefix ${PREFIX} \
    -libdir ${PREFIX}/lib \
    -bindir ${PREFIX}/bin \
    -headerdir ${PREFIX}/include/qt \
    -archdatadir ${PREFIX} \
    -datadir ${PREFIX} \
    -I ${PREFIX}/include \
    -L ${PREFIX}/lib \
    -L ${BUILD_PREFIX}/${HOST}/sysroot/usr/lib64 \
    -R $PREFIX/lib \
    -release \
    -opensource \
    -nomake examples \
    -qt-zlib \
    -qt-harfbuzz \
    -skip qtwebengine \
    -nomake tests


CPATH=$PREFIX/include LD_LIBRARY_PATH=$PREFIX/lib cmake --build   . --parallel
cmake --install .

popd  package_build
