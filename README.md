### Packaging

``` sh
$ conda activate base
$ conda install -c conda-forge mamba boa anaconda-client
```

``` sh
$ mamba init # ONCE
$ mamba activate base
$ mamba build -c jwintz -c conda-forge recipe/1-qt/
$ mamba build -c jwintz -c conda-forge recipe/2-vtk/
```

### Setting Up

``` sh
$ mamba env update -f ./encironment/x-lab.yaml
```
